/*
Copyright (c) 2015, MAURO BIANCO
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * The names of contributors can not be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAURO BIANCO, BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SILENT
#define EVAL(X) std::cout << #X << ": " << X << std::endl
#define EVAL_P(S, X) std::cout << S << ": " << #X << ": " << X << std::endl
#else
#define EVAL(x)
#define EVAL_P(S, x)
#endif

#include <iostream>
#include <bitvector.h>
#include <cassert>
#include <stdlib.h>
#include <algorithm>

template <typename b_type>
bool test() {

    static const int length = 193;
    bitvector<b_type> v1(length);
    bitvector<b_type> v2(length);
    bitvector<b_type> v3(length);

//     for (std::size_t s = 0; s<v1.size(); ++s)  {

//         for (size_t i = 0; i<v1.size(); ++i) {
//             v1[i] = (i==s)?1:0;
//         }

//         for (size_t i = 0; i<v1.size(); ++i) {
//             assert(v1[i] == (i==s)?1:0);
//         }
//     }

//     for (size_t i = 0; i<v1.size(); ++i) {
//         v1[i] = rand();
//         v2[i] = ~v1[i];
//     }

//     for (size_t i = 0; i<v1.size(); ++i) {
//         //std::cout << v1[i];
//         assert(v1[i] != v2[i]);
//     }
//     //std::cout << std::endl;


//     for (size_t i = 0; i<v1.size(); ++i) {
//         v1[i] = rand();
//         v3[i] = v1[i];
//         v2[i] = rand();
//     }

//     v1 &= v2;
//     for (size_t i = 0; i<v1.size(); ++i) {
//         //std::cout << v1[i];
//         assert((v1[i] == (v3[i] & v2[i])));
//     }
//     //std::cout << std::endl;

//     for (size_t i = 0; i<v1.size(); ++i) {
//         v1[i] = rand();
//         v3[i] = v1[i];
//         v2[i] = rand();
//     }

//     v1 |= v2;
//     for (size_t i = 0; i<v1.size(); ++i) {
//         //std::cout << v1[i];
//         assert((v1[i] == (v3[i] | v2[i])));
//     }
//     //std::cout << std::endl;

//     v1=v2;
//     ~v2;

//     for (size_t i = 0; i<v1.size(); ++i) {
//         assert(v1[i] == (!v2[i]));
//     }

//     EVAL(v1.size());
//     for (std::size_t start = 0; start<2*v1.size(); ++start) {

//         auto bl = v1.make_block(start);

//         EVAL(bl);
//         EVAL(v1);
//         EVAL(start);
//         static const int bbs = decltype(v1)::BitBlockSize;
//         const std::size_t bound = (start+bbs<v1.size())?start+bbs:v1.size();
//         for (std::size_t i = start; i < bound ; ++i) {
//             EVAL(i);
//             EVAL(i-start);
//             EVAL(v1[i]);
//             EVAL(bl[i-start]);
//             assert(v1[i] == bl[i-start]);
//         }
//     }


    /// TESTING FFS
    for (size_t i = 0; i<v1.size(); ++i) {
        v1[i] = rand();
    }
//     EVAL(v1);

    int index = 0;
    int next = 0;
    int found = 0;
    while ( (static_cast<std::size_t>(index) < v1.size()) && (next = v1.find_first_set(index)) != 0 ) {
//         EVAL_P("the test", next);
//         EVAL_P("the test", index);
        assert(v1[next-1] == 1);
        index = next;
        ++found;
    }

    int count = 0;
    for (size_t i = 0; i<v1.size(); ++i) {
        count += v1[i];
    }
//     EVAL(found);
//     EVAL(count);
    assert(count == found);

    return true;
}


int main() {


    /* For some reason only two tests at the time works without getting into an infinite loop */
    test<unsigned int>();
    test<unsigned long long int>();
    //test<unsigned long int>();
    //test<unsigned short>();
    return 0;
}
