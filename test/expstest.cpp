/*
Copyright (c) 2015, MAURO BIANCO
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * The names of contributors can not be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAURO BIANCO, BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SILENT
#define EVAL(X) std::cout << #X << ": " << X << std::endl
#define EVAL_P(S, X) std::cout << S << ": " << #X << ": " << X << std::endl
#else
#define EVAL(x)
#define EVAL_P(S, x)
#endif

#include <iostream>
#include <bitvector.h>
#include <cassert>
#include <stdlib.h>
#include <algorithm>
#include <expressions.h>

using namespace expressions;

template <typename b_type>
bool test() {

    static const int length = 193;
    bitvector<b_type> v1(length);
    bitvector<b_type> v2(length);
    bitvector<b_type> v3(length);
    bitvector<b_type> v4(length);

    for (size_t i = 0; i<v1.size(); ++i) {
        v1[i] = 0;
        v2[i] = rand();
        v3[i] = rand();
        v4[i] = rand();
    }
    
    EVAL(v2);
    EVAL(v3);
#ifndef SILENT
    std::cout << And(v2,v3) << std::endl;
#endif
    EVAL(v2);
    EVAL(v3);
    
    EVAL(v2);
    EVAL(v3);
    EVAL(v1);
    eval (And(v2, v3), v1);
    EVAL(v2);
    EVAL(v3);
    EVAL(v1);

    for (size_t i=0; i < v1.size(); ++i) {
        assert(v1[i] == (v2[i] & v3[i]));
    }

    eval (Not(And(v2, v3)), v1);
    EVAL(v1);

    for (size_t i=0; i < v1.size(); ++i) {
        assert(v1[i] == !(v2[i] & v3[i]));
    }


    eval (Or(v4, Not(And(v2, v3))), v1);

    EVAL(v4);
    EVAL(v2);
    EVAL(v3);
    EVAL(v1);
#ifndef SILENT
    std::cout << Or(v4, Not(And(v2, v3))) << std::endl;
#endif
    for (size_t i=0; i < v1.size(); ++i) {
        assert(v1[i] == (v4[i]|(!(v2[i] & v3[i]))));
    }

    eval (Or(v4, Not(Xor(v2, v3))), v1);

    EVAL(v4);
    EVAL(v2);
    EVAL(v3);
    EVAL(v1);
#ifndef SILENT
    std::cout << Or(v4, Not(And(v2, v3))) << std::endl;
#endif
    for (size_t i=0; i < v1.size(); ++i) {
        assert(v1[i] == (v4[i]|(!(v2[i] ^ v3[i]))));
    }

 
    ///////// ASSIGNMENT
    
    v1 = And(v2, v3);

    for (size_t i=0; i < v1.size(); ++i) {
        assert(v1[i] == (v2[i] & v3[i]));
    }

    v1 = Not(And(v2, v3));

    for (size_t i=0; i < v1.size(); ++i) {
        assert(v1[i] == !(v2[i] & v3[i]));
    }


    v1 = Or(v4, Not(And(v2, v3)));

    for (size_t i=0; i < v1.size(); ++i) {
        assert(v1[i] == (v4[i]|(!(v2[i] & v3[i]))));
    }

    v1 = Or(v4, Not(Xor(v2, v3)));

    for (size_t i=0; i < v1.size(); ++i) {
        assert(v1[i] == (v4[i]|(!(v2[i] ^ v3[i]))));
    }

    //////////// OPERATOR + ASSIGNMENT

    v1 = v2 & v3;

    for (size_t i=0; i < v1.size(); ++i) {
        assert(v1[i] == (v2[i] & v3[i]));
    }

    v1 = ~(v2 & v3);

    for (size_t i=0; i < v1.size(); ++i) {
        assert(v1[i] == !(v2[i] & v3[i]));
    }

    v1 = v4 | (~(v2 & v3));//Or(v4, Not(And(v2, v3)));

    for (size_t i=0; i < v1.size(); ++i) {
        assert(v1[i] == (v4[i]|(!(v2[i] & v3[i]))));
    }

    v1 = v4 | ~(v2 ^ v3);//Or(v4, Not(Xor(v2, v3)));

    for (size_t i=0; i < v1.size(); ++i) {
        assert(v1[i] == (v4[i]|(!(v2[i] ^ v3[i]))));
    }

    for (std::size_t shft=0; shft<=v1.size(); ++shft){
#ifndef SILENT
        std::cout << " -------------------------------------->>>>> " << shft << std::endl;
#endif
        EVAL(v4);
        EVAL(v2);
        v1 = v4 | (v2<<shft);//Or(v4, Not(Xor(v2, v3)));
        EVAL(v1);
        EVAL(v2);
        for (size_t i=0; i < v1.size(); ++i) {
            assert(v1[i] == (v4[i]|(v2[i+shft])));
        }
    }

    for (std::size_t shft=0; shft<=v1.size(); ++shft){
#ifndef SILENT
        std::cout << "RShift  -------------------------------------->>>>> " << shft << std::endl;
#endif
        EVAL(v4);
        EVAL(v2);
        v1 = v4 | (v2>>shft);//Or(v4, Not(Xor(v2, v3)));
        EVAL(v1);
        EVAL(v2);
        for (size_t i=0; i < v1.size(); ++i) {
            assert(v1[i] == (v4[i]|(v2[i-shft])));
        }
    }

   return true;
}

int main() {

    test<unsigned int>();
    test<unsigned long long int>();
    test<unsigned long int>();
    test<unsigned short>();
    return 0;
}
