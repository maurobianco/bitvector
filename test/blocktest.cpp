/*
Copyright (c) 2015, MAURO BIANCO
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * The names of contributors can not be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAURO BIANCO, BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SILENT
#define EVAL(X) std::cout << #X << ": " << X << std::endl
#else
#define EVAL(x)
#endif

#include <iostream>
#include <bitblock.h>
#include <cassert>
#include <stdlib.h>


template <typename b_type>
bool test_block() {

    block<b_type> b;

    const int l = sizeof(b_type)*8;
    EVAL(l);
    b_type value = (static_cast<b_type>(1))<<(l/2+1);

    b = value;

    assert(b.b == value);
    EVAL(b.b);
    EVAL(b.find_first_set());
    EVAL(b.find_first_set(l/2-2));
    EVAL(b.find_first_set(l/2));
    EVAL(b.find_first_set(l/2+1));
    EVAL(b.find_first_set(l/2+2));

    assert(b.find_first_set() == l/2+2);
    assert(b.find_first_set(l/2-2) == 4);
    assert(b.find_first_set(l/2) == 2);
    assert(b.find_first_set(l/2+1) == 1);
    assert(b.find_first_set(l/2+2) == 0);

    for (int i = 0; i<static_cast<int>(sizeof(b_type)*8) ; ++i) {
        if (i==l/2+1) {
            assert(b[i] == true);
        } else {
            assert(b[i] == false);
        }
    }

    b_type value2 = ((static_cast<b_type>(1))<<(l/2+1)) + ((static_cast<b_type>(1))<<(l/2+3));

    b = value2;
    EVAL(b.b);
    EVAL(b);
    EVAL(b.find_first_set());
    EVAL(b.find_first_set(l/2-2));
    EVAL(b.find_first_set(l/2));
    EVAL(b.find_first_set(l/2+1));
    EVAL(b.find_first_set(l/2+2));
    EVAL(b.find_first_set(l/2+3));
    EVAL(b.find_first_set(l/2+4));

    assert(b.find_first_set() == l/2+2);
    assert(b.find_first_set(l/2-2) == 4);
    assert(b.find_first_set(l/2) == 2);
    assert(b.find_first_set(l/2+1) == 1);
    assert(b.find_first_set(l/2+2) == 2);
    assert(b.find_first_set(l/2+3) == 1);
    assert(b.find_first_set(l/2+4) == 0);


    for (int i = 0; i<static_cast<int>(sizeof(b_type)*8) ; ++i) {
        switch (i) {
        case l/2+1:
        case l/2+3:
            assert(b[i] == true);
            break;
        default:
            assert(b[i] == false);
        }
    }

    block<b_type> c;
    c[l/2+1] = 1;
    EVAL(c);
    c[l/2+3] = 1;
    EVAL(c);
    c[l/2+5] = 1;
    EVAL(c);
    c[l/2+5] = 0;
    EVAL(c);
    c[l/2-4] = 1;
    EVAL(c);
    c[l/2-4] = 0;
    EVAL(c);

    EVAL(b);
    assert(b==c);

    block<b_type> d(rand()+rand()+rand());
    EVAL(d);

    for (int bl=0; bl<=3; ++bl) {
#ifndef SILENT
        std::cout << "*************************************************************************************************************"
                  << "BLOCK = " << (1<<bl)
                  << " " << __PRETTY_FUNCTION__  << std::endl;
#endif
        block<b_type> e=d;
        block<b_type> f=0;
        EVAL(e);
        EVAL(f);
        for (int i=0; i<l; i+=(1<<bl)) {
            b_type rest = e.lshift(1<<bl);
            f.lshift(1<<bl, rest);
            EVAL(e);
            EVAL(f);
        }
        assert(f==d);
    }

    for (int bl=0; bl<=3; ++bl) {
#ifndef SILENT
        std::cout << "*************************************************************************************************************"
                  << "BLOCK = " << (1<<bl)
                  << " " << __PRETTY_FUNCTION__  << std::endl;
#endif
        block<b_type> e=d;
        block<b_type> f=0;
        EVAL(e);
        EVAL(f);
        for (int i=0; i<l; i+=(1<<bl)) {
            b_type rest = e.rshift(1<<bl);
            f.rshift(1<<bl, rest);
            EVAL(e);
            EVAL(f);
        }
        assert(f==d);
    }


    /* bitwise operations */
#ifndef SILENT
    std::cout << "Bitwise operations" << std::endl;
#endif

    block<b_type> g(rand()+rand()+rand());
    block<b_type> h(rand()+rand()+rand());
    
    b_type rag = g.b & h.b;
    EVAL(g);
    EVAL(h);
    g &= h;
    EVAL(g);
    assert(g.b == rag);

    b_type rog = g.b | h.b;
    g |= h;
    EVAL(g);
    assert(g.b == rog);

    h = block<b_type>(rand()+rand()+rand());

    b_type ng = ~g.b;
    g = ~g;
    EVAL(g);
    assert(g.b == ng);

    b_type nh = ~h.b;
    EVAL(h);
    h = ~h;
    EVAL(h);
    assert(h.b == nh);

    b_type gah = g.b & h.b;;
    EVAL(g);
    EVAL(h);
    block<b_type> gahb = g & h;
    EVAL(gahb);
    assert(gah == gahb.b);

    b_type goh = g.b & h.b;;
    EVAL(g);
    EVAL(h);
    block<b_type> gohb = g & h;
    EVAL(gohb);
    assert(goh == gohb.b);

    EVAL(g);
    EVAL(h);

    return true;
}

int main() {
    test_block<unsigned int>();
    test_block<unsigned long long int>();
    test_block<unsigned int>();
    test_block<short unsigned int>();

    return 0;
}
