CC = g++
CFLAGS = -Iinclude -O3 -std=c++11 -Wall -Wextra -DNOSILENT

HEADERS=include/base_expression.h include/bitblock.h include/bitvector.h include/expressions.h include/proxy.h include/naive_expressions.h

all: expstest vectortest blocktest naiveexpstest bench
	 ./blocktest && ./vectortest && ./expstest && ./naiveexpstest && ./bench 50000000


naiveexpstest: test/naiveexpstest.cpp  $(HEADERS) Makefile
	   $(CC) $(CFLAGS) test/naiveexpstest.cpp -o naiveexpstest 

expstest: test/expstest.cpp  $(HEADERS) Makefile
	   $(CC) $(CFLAGS) test/expstest.cpp -o expstest 

blocktest: test/blocktest.cpp  $(HEADERS) Makefile
	   $(CC) $(CFLAGS) test/blocktest.cpp -o blocktest

vectortest: test/vectortest.cpp  $(HEADERS) Makefile
	   $(CC) $(CFLAGS) test/vectortest.cpp -o vectortest

bench: benchmark/benchmark.cpp benchmark/benchmark.h $(HEADERS) Makefile
	   $(CC) $(CFLAGS) benchmark/benchmark.cpp -o bench

clean:
		rm vectortest expstest naiveexpstest blocktest bench
