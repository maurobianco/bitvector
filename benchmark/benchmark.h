/*
Copyright (c) 2015, MAURO BIANCO
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * The names of contributors can not be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAURO BIANCO, BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

template <typename b_type>
bool test(int length, int prefix) {

    bitvector<b_type> v1(length);
    bitvector<b_type> v2(length);
    bitvector<b_type> v3(length);
    bitvector<b_type> v4(length);

    for (size_t i = 0; i<v1.size(); ++i) {
        v1[i] = 0;
        v2[i] = rand();
        v3[i] = rand();
        v4[i] = rand();
    }


    gettimeofday(&start_tv, NULL);
    v1 = v4 | (((v3) & (v2)) ^ (v3 ^v4));
    gettimeofday(&stop_tv, NULL);
    lapse_time = ((static_cast<double>(stop_tv.tv_sec)+1/1000000.0*static_cast<double>(stop_tv.tv_usec)) - (static_cast<double>(start_tv.tv_sec)+1/1000000.0*static_cast<double>(start_tv.tv_usec))) * 1000.0;
    std::cout << prefix << " -1 " << " " << sizeof(b_type)*8 << " " << lapse_time  << std::endl;

    for (std::size_t shft=1; shft<=64; shft+=17){

        gettimeofday(&start_tv, NULL);
        v1 = v4 | (((v3<<shft) & (v2>>shft)) ^ ((v3>>shft) ^ (v4)<<shft));
        gettimeofday(&stop_tv, NULL);
        lapse_time = ((static_cast<double>(stop_tv.tv_sec)+1/1000000.0*static_cast<double>(stop_tv.tv_usec)) - (static_cast<double>(start_tv.tv_sec)+1/1000000.0*static_cast<double>(start_tv.tv_usec))) * 1000.0;
        std::cout << prefix << " " << shft << " " << sizeof(b_type)*8 << " " << lapse_time  << std::endl;
        for (size_t i=0; i < v1.size(); ++i) {
            assert(v1[i] == (v4[i]| ( (v3[i+shft]&v2[i-shft]) ^ (v3[i-shft] ^ v4[i+shft])) ));
        }
    }
    
    return true;
}

