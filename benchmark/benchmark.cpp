/*
Copyright (c) 2015, MAURO BIANCO
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * The names of contributors can not be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAURO BIANCO, BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SILENT
#define EVAL(X) std::cout << #X << ": " << X << std::endl
#define EVAL_P(S, X) std::cout << S << ": " << #X << ": " << X << std::endl
#else
#define EVAL(x)
#define EVAL_P(S, x)
#endif

#include <iostream>
#include <cassert>
#include <stdlib.h>
#include <algorithm>
#include <bitvector.h>
#include <expressions.h>
#include <naive_expressions.h>
#include <sys/time.h>

struct timeval start_tv;
struct timeval stop_tv;
double lapse_time;

namespace expressions {
#include "benchmark.h"
}
namespace naive_expressions {
#include "benchmark.h"
}

int main(int argc, char** argv) {

    int l = atoi(argv[1]);

    expressions::test<unsigned int>(l,0);
    expressions::test<unsigned long long int>(l,0);
    expressions::test<unsigned long int>(l,0);
    expressions::test<unsigned short>(l,0);

    naive_expressions::test<unsigned int>(l,1);
    naive_expressions::test<unsigned long long int>(l,1);
    naive_expressions::test<unsigned long int>(l,1);
    naive_expressions::test<unsigned short>(l,1);
    return 0;
}
