#pragma once

/*
Copyright (c) 2015, MAURO BIANCO
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * The names of contributors can not be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAURO BIANCO, BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "base_expression.h"
#include "bitblock.h"
#include <vector>
#include <cassert>

template <typename blocktype>
struct bitvector {
    typedef blocktype raw_block_type;
    typedef block<blocktype> block_type;
    typedef bitvector<raw_block_type> this_type;

    static const std::size_t BlockSize = sizeof(blocktype);
    static const std::size_t BitBlockSize = sizeof(blocktype)*8;
    std::size_t _size;
    std::vector<block_type> _vector;
    
    bitvector(std::size_t size)
        : _size(size)
        , _vector( static_cast<std::size_t>(((size/BitBlockSize)*BitBlockSize)) != size?
                   size/BitBlockSize+1:
                   size/BitBlockSize)
    {}

    std::size_t size() const {return _size;}

    std::size_t block_size() const {return _vector.size();}

    /** Set the ith bit to value&1

        \param index The index
        \param value The value
    */
    void set(int index, int value) {
        int block = blk_shift(index);
        int offset = offs_shift(index);
        _vector[block].set(offset, value);
    }

    /** Get the value at index

        \param index The index
        \return The value
    */
    int get(int index) const {
        if ((index<0) || (index >= static_cast<int>(size()))) {
            //std::cout << "o";
            return 0;
        }
        int block = blk_shift(index);
        int offset = offs_shift(index);
        return _vector[block].get(offset);
    }

    /**
       Returns a block_proxy to set the value os a bit

       \param pos Index of the value to pick
       \return The block_proxy to the value requested
     */
    _detail::proxy<this_type> operator[](int pos) {
        return _detail::proxy<this_type>(*this, pos);
    }

    int operator[](int pos) const {
        return get(pos);
    }


    this_type operator=(this_type const& other) {
        _vector = other._vector;
        return *this;
    }

    template<typename T>
    this_type& operator=(expressions::expression<T> const& ex) {
        eval(ex, *this);
        return *this;
    }

    /**
       Returns the index of the first bit set to 1
       starting at the index specified by the argument.
       As for ffs, the indices start with 1.

       \param pos Index where to start the search for the set bit
       \return The index of the ffs or 0 if not found
    */
    int find_first_set(int i = 0) const {

        int result = 0;
        int blk = blk_shift(i);
        int first = get_block(blk).find_first_set(offs_shift(i));

        if ( first != 0 ) {
            return i+first;
        } else {
            ++blk;
            int rest = BitBlockSize - (i%BitBlockSize);
            while ( blk < block_size() && (result = get_block(blk).find_first_set()) == 0 ) {
                ++blk;
            }
            if (blk < block_size()) {
                result += rest;
            }
        }
        return i+result;
    }

    /******* BITWISE OPERATIONS *********/
    
    this_type& operator&=(bitvector<blocktype> const& otr) {
        assert(size() == otr.size());
        auto it = _vector.begin();
        auto it2 = otr._vector.begin();
        for ( ;
             it < _vector.end(); 
             ++it, ++it2) {
            (*it) &= (*it2);
        }
        return *this;
    }

    this_type& operator|=(bitvector<blocktype> const& otr) {
        assert(size() == otr.size());
        auto it = _vector.begin();
        auto it2 = otr._vector.begin();
        for ( ; 
             it < _vector.end(); 
             ++it, ++it2) {
            (*it) |= (*it2);
        }
        return *this;
    }

    this_type& operator~() {
        auto it = _vector.begin();
        for ( ; 
             it < _vector.end(); 
             ++it) {
            (*it) = ~(*it);
        }
        return *this;
    }
    
    this_type& operator!() {
        return ~(*this);
    }


    /* BLOCK OPERATIONS */
    block_type& get_block(std::size_t i) {
        return _vector[i];
    }

    block_type get_block(std::size_t i) const {
        return _vector[i];
    }

    /** Get the block long sub sequence starting at pos, but aligned ina block
        for efficiency

        \param pos The index where to start getting the block
    */
    block_type make_block(const int pos) const {
        int blk = blk_shift(pos);
        int offs = offs_shift(pos);

        block_type x=0, y=0; // zero!

        if ((blk>=0) && (blk < static_cast<int>(_vector.size()))) {
            x = _vector[blk];
        }

        // TODO try to avoid loading if offs == 0
        if ((blk>=-1) && (blk < static_cast<int>(_vector.size())-1)) {
            y = _vector[blk+1];
        }


        if (offs) {
            raw_block_type z = y.lshift(offs);
            x.lshift(offs,z);
        }

        return x;
    }

private:
    int blk_shift(int i) const {
        return i/BitBlockSize;
    }

    int offs_shift(int i) const {
        return i%BitBlockSize;
    }
};

template <typename BT>
std::ostream& operator<<(std::ostream& s, bitvector<BT> const& v1) {
//     s << "\n";
    for (std::size_t i = 0; i<v1.size(); ++i) {
        if ( (i % bitvector<BT>::BitBlockSize) == 0 ) {
            s << ":";
        }
        s << v1[i];
    }
//     s << "\n";
//     for (int i = 0; i<v1.size(); ++i) {
//         if ( (i % bitvector<BT>::BitBlockSize) == 0 ) {
//             s << ":";
//         }
//         s << i%10;
//     }
    return s;
}

