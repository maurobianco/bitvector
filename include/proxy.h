#pragma once

namespace _detail {
    template <typename T>
    struct proxy {
        int pos;
        T & bl;
        proxy(T &bl, int pos) : pos(pos), bl(bl) {}

        proxy<T> operator=(proxy<T>);

        proxy<T> operator=(int);

        operator typename T::raw_block_type() const {
            return bl.get(pos);
        }
    };

    /** Implementation of the random access operator for the block
     */
    template <typename T>
    inline _detail::proxy<T> _detail::proxy<T>::operator=(int value) {
        this->bl.set(this->pos, value);
        return *this;
    }

    /** Implementation of the random access operator for the block
     */
    template <typename T>
    inline _detail::proxy<T> _detail::proxy<T>::operator=(proxy<T> value) {
        this->bl.set(this->pos, static_cast<typename T::raw_block_type>(value));
        return *this;
    }
}
