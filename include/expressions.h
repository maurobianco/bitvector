#pragma once

/*
  Copyright (c) 2015, MAURO BIANCO
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  * Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
  * The names of contributors can not be used to endorse or promote products
  derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL MAURO BIANCO, BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <type_traits>
#include "base_expression.h"
#include "bitvector.h"


namespace expressions {
    template <typename Op1>
    struct _Reference;

    template <typename T>
    struct is_bit_vector : std::false_type {};

    template <typename T>
    struct is_bit_vector<bitvector<T>> : std::true_type {};

    template <typename Op1, typename Op2>
    struct binary_op: public expression<typename Op1::block_type> {
        using block_type = typename Op1::block_type;

        Op1  op1;
        Op2 op2;

        binary_op(Op1 const& op1, Op2 const& op2)
            : op1(op1)
            , op2(op2)
        {
            //         EVAL_P("binary_op constructor 1", op1.get_op1());
            //         EVAL_P("binary_op constructor 2", op2.get_op1());
        }

        Op1 const& get_op1() const {
            return op1;
        }

        Op2 const& get_op2() const {
            return op2;
        }

        int lbound() const {
            return std::max(op1.lbound(), op2.lbound());
        }

        int hbound() const {
            return std::min(op1.hbound(), op2.hbound());
        }

    };

    template <typename Op1>
    struct unary_op : public expression<typename Op1::block_type> {
        using block_type = typename Op1::block_type;

        Op1 op1;

        unary_op(Op1 const& op1)
            : op1(op1)
        {}

        Op1 const& get_op1() const {
            return op1;
        }

        int lbound() const {
            return op1.lbound();
        }

        int hbound() const {
            return op1.hbound();
        }
    };

    template <typename Op1, typename Op2>
    struct _And : public binary_op<Op1, Op2> {
        using binary_op<Op1, Op2>::binary_op;

        using block_type = typename binary_op<Op1,Op2>::block_type;

        block_type eval(int i) const {
            return (binary_op<Op1, Op2>::get_op1().eval(i)) & (binary_op<Op1, Op2>::get_op2().eval(i));
        }
    };

    template <typename Op1, typename Op2>
    struct _Or : public binary_op<Op1, Op2> {
        using binary_op<Op1, Op2>::binary_op;

        using block_type = typename binary_op<Op1,Op2>::block_type;

        block_type eval(int i) const {
            return (binary_op<Op1, Op2>::get_op1().eval(i)) | (binary_op<Op1, Op2>::get_op2().eval(i));
        }
    };

    template <typename Op1, typename Op2>
    struct _Xor : public binary_op<Op1, Op2> {
        using binary_op<Op1, Op2>::binary_op;

        using block_type = typename binary_op<Op1,Op2>::block_type;

        block_type eval(int i) const {
            return (binary_op<Op1, Op2>::get_op1().eval(i)) ^ (binary_op<Op1, Op2>::get_op2().eval(i));
        }
    };

    template <typename Op1>
    struct _Not  : public unary_op<Op1> {
        using unary_op<Op1>::unary_op;

        using block_type = typename unary_op<Op1>::block_type;

        block_type eval(int i) const {
            block_type x = unary_op<Op1>::get_op1().eval(i);
            return ~x;
        }
    };

    template <typename Op1>
    struct _Reference : public expression<typename Op1::block_type> {
        using block_type = typename Op1::block_type;

        //    static_assert(std::is_same<bitvector<typename Op1::raw_block_type>, Op1>::value, "aaaaa");
        Op1 const& op1;
 
        _Reference(Op1 const& op1)
            : op1(op1)
        {
            //EVAL_P("_Reference constructor", op1);
        }

        Op1 const& get_op1() const {
            return op1;
        }

        int lbound() const {
            return 0;
        }

        int hbound() const {
            return op1.size();
        }

        block_type eval(int i) const {
            return op1.get_block(i);
        }

    };

    template <typename Op1>
    struct _RShift : public _Reference<Op1> {
        int pos;

        using block_type = typename _Reference<Op1>::block_type;
        using _Reference<Op1>::op1;

        _RShift(Op1 const& op1, int pos)
            : _Reference<Op1>(op1)
            , pos(pos)
        {}

        int lbound() const {
            return op1.size()-pos;
        }

        int hbound() const {
            return 0;
        }

        block_type eval(int i) const {
            return _Reference<Op1>::get_op1().make_block(i*(Op1::BitBlockSize)-pos);
        }
    };

    template <typename Op1>
    struct _LShift : public _Reference<Op1> {
        int pos;

        using block_type = typename _Reference<Op1>::block_type;
        using _Reference<Op1>::op1;

        _LShift(Op1 const& op1, int pos)
            : _Reference<Op1>(op1)
            , pos(pos)
        {}

        int lbound() const {
            return pos;
        }

        int hbound() const {
            return op1.size();
        }

        block_type eval(int i) const {
            return _Reference<Op1>::get_op1().make_block(i*(Op1::BitBlockSize)+pos);
        }
    };


    namespace _detail {
        template <typename T>
        struct is_operator: std::false_type {};

        template <typename T1, typename T2>
        struct is_operator<_And<T1, T2> > : std::true_type {};

        template <typename T1, typename T2>
        struct is_operator<_Xor<T1, T2> > : std::true_type {};

        template <typename T1, typename T2>
        struct is_operator<_Or<T1, T2> > : std::true_type {};

        template <typename T>
        struct is_operator<_Not<T> > : std::true_type {};

        template <typename T>
        struct is_operator<_RShift<T> > : std::true_type {};

        template <typename T>
        struct is_operator<_LShift<T> > : std::true_type {};

        template <typename T>
        struct put_ref;

        template <typename T1, typename T2>
        struct put_ref<_And<T1, T2> > {
            using type = _And<T1,T2>;
        };

        template <typename T1, typename T2>
        struct put_ref<_Or<T1, T2> > {
            using type = _Or<T1,T2>;
        };

        template <typename T1, typename T2>
        struct put_ref<_Xor<T1, T2> > {
            using type = _Xor<T1,T2>;
        };

        template <typename T>
        struct put_ref<_Not<T> > {
            using type = _Not<T>;
        };

        template <typename T>
        struct put_ref<_RShift<T> > {
            using type = _RShift<T>;
        };

        template <typename T>
        struct put_ref<_LShift<T> > {
            using type = _LShift<T>;
        };

        template <typename T>
        struct put_ref<bitvector<T>> {
            using type = _Reference<bitvector<T>>;
        };

        template <typename T1, typename T2, typename Enable = void>
        struct good_arguments: std::false_type {};

        template <typename T>
        struct good_arguments<bitvector<T>, bitvector<T> >: std::true_type {};

        template <typename T1, typename T2>
        struct good_arguments<bitvector<T1>, T2,
                              typename std::enable_if<is_operator<T2>::value >::type> : std::true_type {};

        template <typename T1, typename T2>
        struct good_arguments<T1, bitvector<T2>,
                              typename std::enable_if<is_operator<T1>::value >::type> : std::true_type {};

        template <typename T1, typename T2>
        struct good_arguments<T1, T2, typename std::enable_if<is_operator<T1>::value && 
                                                              is_operator<T2>::value >::type >: std::true_type {};

        template <typename T, typename Enable = void>
        struct good_argument: std::false_type {};

        template <typename T>
        struct good_argument<bitvector<T> >: std::true_type {};

        template <typename T>
        struct good_argument<T, typename std::enable_if<is_operator<T>::value >::type >: std::true_type {};
    } // namespace _detail

    template <typename Op1, typename Op2>
    _And<typename _detail::put_ref<Op1>::type,typename _detail::put_ref<Op2>::type> 
    And(Op1 const& op1, Op2 const& op2) {
        return _And<typename _detail::put_ref<Op1>::type,typename _detail::put_ref<Op2>::type>
            (typename _detail::put_ref<Op1>::type(op1), 
             typename _detail::put_ref<Op2>::type(op2));
    }

    template <typename Op1, typename Op2>
    _Or<typename _detail::put_ref<Op1>::type,typename _detail::put_ref<Op2>::type> 
    Or(Op1 const& op1, Op2 const& op2) {
        return _Or<typename _detail::put_ref<Op1>::type,typename _detail::put_ref<Op2>::type>
            (typename _detail::put_ref<Op1>::type(op1), 
             typename _detail::put_ref<Op2>::type(op2));
    }

    template <typename Op1, typename Op2>
    _Xor<typename _detail::put_ref<Op1>::type,typename _detail::put_ref<Op2>::type> 
    Xor(Op1 const& op1, Op2 const& op2) {
        return _Xor<typename _detail::put_ref<Op1>::type,typename _detail::put_ref<Op2>::type>
            (typename _detail::put_ref<Op1>::type(op1), 
             typename _detail::put_ref<Op2>::type(op2));
    }

    template <typename Op1>
    _Not<typename _detail::put_ref<Op1>::type>
    Not(Op1 const& op1) {
        return _Not<typename _detail::put_ref<Op1>::type>(typename _detail::put_ref<Op1>::type(op1));
    }

    template <typename Op1>
    _RShift<Op1>
    RShift(Op1 const& op1, int pos) {
        return _RShift<Op1>(op1, pos);
    }

    template <typename Op1>
    _LShift<Op1>
    LShift(Op1 const& op1, int pos) {
        return _LShift<Op1>(op1, pos);
    }

    //////////////////////////////////////////////
    ///// OPERATOR VERSION ///////////////////////
    //////////////////////////////////////////////

    template <typename Op1, typename Op2>
    typename std::enable_if<_detail::good_arguments<Op1,Op2>::value,
                            _And<typename _detail::put_ref<Op1>::type,
                                 typename _detail::put_ref<Op2>::type> >::type
    operator&(Op1 const& op1, Op2 const& op2) {
        return And(op1, op2);
    }

    template <typename Op1, typename Op2>
    typename std::enable_if<_detail::good_arguments<Op1,Op2>::value,
                            _Or<typename _detail::put_ref<Op1>::type,
                                typename _detail::put_ref<Op2>::type> >::type
    operator|(Op1 const& op1, Op2 const& op2) {
        return Or(op1, op2);
    }

    template <typename Op1, typename Op2>
    typename std::enable_if<_detail::good_arguments<Op1,Op2>::value,
                            _Xor<typename _detail::put_ref<Op1>::type,
                                 typename _detail::put_ref<Op2>::type> >::type
    operator^(Op1 const& op1, Op2 const& op2) {
        return Xor(op1, op2);
    }


    template <typename Op1>
    typename std::enable_if<_detail::good_argument<Op1>::value,
                            _Not<typename _detail::put_ref<Op1>::type> >::type
    operator~(Op1 const& op1) {
        return Not(op1);
    }

    template <typename Op1>
    typename std::enable_if<_detail::good_argument<Op1>::value,
                            _RShift<Op1> >::type
    operator>>(Op1 const& op1, int pos) {
        return RShift(op1, pos);
    }

    template <typename Op1>
    typename std::enable_if<_detail::good_argument<Op1>::value,
                            _LShift<Op1> >::type
    operator<<(Op1 const& op1, int pos) {
        return LShift(op1, pos);
    }

    //////////////////////////////////////////
    ////////////// OUTPUT
    //////////////////////////////////////////

    template <typename Op1, typename Op2>
    std::ostream& operator<<(std::ostream& s, _And<Op1,Op2> const& x) {
        s << "And(" << x.get_op1() << ", " << x.get_op2() << ")";
        return s;
    }

    template <typename Op1, typename Op2>
    std::ostream& operator<<(std::ostream& s, _Or<Op1,Op2> const& x) {
        s << "Or(" << x.get_op1() << ", " << x.get_op2() << ")";
        return s;
    }

    template <typename Op1, typename Op2>
    std::ostream& operator<<(std::ostream& s, _Xor<Op1,Op2> const& x) {
        s << "Xor(" << x.get_op1() << ", " << x.get_op2() << ")";
        return s;
    }

    template <typename Op1>
    std::ostream& operator<<(std::ostream& s, _Not<Op1> const& x) {
        s << "Not(" << x.get_op1() << ")";
        return s;
    }

    template <typename Op1>
    std::ostream& operator<<(std::ostream& s, _Reference<Op1> const& x) {
        s << "Ref{" << x.get_op1() << "}";
        return s;
    }
} // namespace expressions
