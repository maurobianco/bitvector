#pragma once

/*
Copyright (c) 2015, MAURO BIANCO
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * The names of contributors can not be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAURO BIANCO, BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "bitvector.h"

namespace naive_expressions {
    template <typename T>
    inline bitvector<T> operator&(bitvector<T> const& x, bitvector<T> const& y) {
        assert(x.size() == y.size());
        bitvector<T> r(x.size());
        for (std::size_t i = 0; i < x.block_size(); ++i) {
            r.get_block(i) = x.get_block(i) & y.get_block(i);
        }
        return (r);
    }

    template <typename T>
    inline bitvector<T> operator|(bitvector<T> const& x, bitvector<T> const& y) {
        assert(x.size() == y.size());
        bitvector<T> r(x.size());
        for (std::size_t i = 0; i < x.block_size(); ++i) {
            r.get_block(i) = x.get_block(i) | y.get_block(i);
        }
        return (r);
    }

    template <typename T>
    inline bitvector<T> operator^(bitvector<T> const& x, bitvector<T> const& y) {
        assert(x.size() == y.size());
        bitvector<T> r(x.size());
        for (std::size_t i = 0; i < x.block_size(); ++i) {
            r.get_block(i) = x.get_block(i) ^ y.get_block(i);
        }
        return (r);
    }

    template <typename T>
    inline bitvector<T> operator~(bitvector<T> const& x) {
        bitvector<T> r(x.size());
        for (std::size_t i = 0; i < x.block_size(); ++i) {
            r.get_block(i) = ~(x.get_block(i));
        }
        return (r);
    }

    template <typename T>
    inline bitvector<T> operator>>(bitvector<T> const& x, int pos) {
        bitvector<T> r(x.size());
        for (std::size_t i = 0; i < x.block_size(); ++i) {
            r.get_block(i) = x.make_block(i*(bitvector<T>::BitBlockSize)-pos);
        }
        return (r);
    }

    template <typename T>
    inline bitvector<T> operator<<(bitvector<T> const& x, int pos) {
        bitvector<T> r(x.size());
        for (std::size_t i = 0; i < x.block_size(); ++i) {
            r.get_block(i) = x.make_block(i*(bitvector<T>::BitBlockSize)+pos);
        }
        return (r);
    }

    template <typename T>
    inline bitvector<T> And(bitvector<T> const& x, bitvector<T> const& y) {
        return (x&y);
    }

    template <typename T>
    inline bitvector<T> Or(bitvector<T> const& x, bitvector<T> const& y) {
        return (x|y);
    }

    template <typename T>
    inline bitvector<T> Xor(bitvector<T> const& x, bitvector<T> const& y) {
        return (x^y);
    }

    template <typename T>
    inline bitvector<T> Not(bitvector<T> const& x) {
        return (~x);
    }

    template <typename T>
    inline bitvector<T> RShift(bitvector<T> const& x, int pos) {
        return (x>>pos);
    }

    template <typename T>
    inline bitvector<T> LShift(bitvector<T> const& x, int pos) {
        return (x<<pos);
    }
    
} // namespace naive_expressions
