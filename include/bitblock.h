#pragma once

/*
Copyright (c) 2015, MAURO BIANCO
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * The names of contributors can not be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MAURO BIANCO, BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <proxy.h>
#include <vector>
#include <strings.h>
#include <ostream>

template <typename blocktype>
struct block;


namespace _detail {
    inline int get_ffs(unsigned long long int x) {
        return ffsll(x);
    }

    inline int get_ffs(unsigned int x) {
        return ffs(x);
    }

    inline int get_ffs(unsigned long int x) {
        return ffsl(x);
    }

    inline int get_ffs(long long int x) {
        return ffsll(x);
    }

    inline int get_ffs(int x) {
        return ffs(x);
    }

    inline int get_ffs(long int x) {
        return ffsl(x);
    }

    inline int get_ffs(short x) {
        return ffs(x);
    }

    inline int get_ffs(unsigned short x) {
        return ffs(x);
    }


    /** Computes the binary of a value as a string

        \param x The value to make binary
        \return String with binary representation, first bit on the left
    */
    template <typename T>
    inline std::string binary_of(T x) {
        std::string b = "";
        for (int i = 0; i<static_cast<int>(sizeof(T)*8); ++i) {
            b = b + std::to_string((x&1));
            x = x >> 1;
        }
        return b;
    }
}

/**
   class that implements the basic block of a vector of bits
 */
template <typename blocktype>
struct block {

    typedef blocktype raw_block_type;
    typedef block<blocktype> this_type;

    blocktype b;

    /**
       This constructor set the sequenze of the block to a specific value.

       The first bit of the sequence is the least significant one.
     */
    block(blocktype b) 
        : b(b) 
    {}

    /**
       Default constructor sets all bits to '0'
     */
    block() 
        : b(0) 
    {}

    /**
       Assigns the values of the block to the specific rvalue.

       The first bit is the least significant one.

       \param t The value to assign to the sequence
       \return Returns the block as reference
     */
    block& operator=(blocktype t) {
        b = t;
        return *this;
    }

    /**
       Returns the index of the first bit set to 1

       \return The index of the ffs or 0 if not found
     */
    int find_first_set() const {
        
        return _detail::get_ffs(b);
    }

    /**
       Returns the index of the first bit set to 1
       starting at the index specified by the argument.

       \param pos Index where to start the search for the set bit
       \return The index of the ffs or -1 if not found
    */
    int find_first_set(int pos) const {
        return (b>>pos)?_detail::get_ffs((b>>pos)/*<<pos*/):0;
    }

    /**
       Returns a proxy to set the value os a bit

       \param pos Index of the value to pick
       \return The proxy to the value requested
     */
    _detail::proxy<this_type> operator[](int pos) {
        return _detail::proxy<this_type>(*this, pos);
    }

    /** Set the ith bit to value&1

        \param index The index
        \param value The value
    */
    void set(int index, int value) {
        blocktype mask = ~((static_cast<blocktype>(1))<<index);
        b = b&mask;
        b = b|((static_cast<blocktype>(value&1))<<index);
    }

    /** Get the value at index

        \param index The index
        \return The value
    */
    int get(int index) const {
        return (b>>index)&1;
    }

    /** Comparison between blocks
     */
    bool operator==(block<blocktype> const& other) const {
        return b == other.b;
    }

    /** Shift the block 'pos' positions to the left (does it make sense to call it this way?),
        inserting on the "empty" positions on the right the value in "insert".
        
        It returns the bits extracted on the left side.
        
        \param pos Number of positions to shift
        \param insert Value to insert/push on the right
        \return Value extracted on the left
    */
    blocktype lshift(int pos, blocktype insert=0) {
        blocktype mask = (((static_cast<blocktype>(1))<<pos)-1);
        blocktype rest = b&mask;
        b = b >> pos;
        insert = insert << (sizeof(blocktype)*8-pos);
        b = b | insert;
        return rest;
    }

    /** Shift the block 'pos' positions to the right (does it make sense to call it this way?),
        inserting on the "empty" positions on the left the value in "insert".
        
        It returns the bits extracted on the right side.
        
        \param pos Number of positions to shift
        \param insert Value to insert/push on the left
        \return Value extracted on the right
    */
    blocktype rshift(int pos, blocktype insert=0) {
        blocktype mask = (((static_cast<blocktype>(1))<<pos)-1);
        mask = mask << (sizeof(blocktype)*8-pos);
        blocktype rest = (b&mask)>>(sizeof(blocktype)*8-pos);
        b = b << pos;
        b = b | insert;
        return rest;
    }

    /******* BITWISE OPERATIONS *********/
    
    block<blocktype>& operator&=(block<blocktype> const& otr) {
        b = b & otr.b;
        return *this;
    }

    block<blocktype>& operator|=(block<blocktype> const& otr) {
        b = b | otr.b;
        return *this;
    }
};


template <typename blocktype>
inline block<blocktype> operator&(block<blocktype> const& one, block<blocktype> const& otr) {
    return block<blocktype>(one.b & otr.b);
}

template <typename blocktype>
inline block<blocktype> operator|(block<blocktype> const& one, block<blocktype> const& otr) {
    return block<blocktype>(one.b | otr.b);
}

template <typename blocktype>
inline block<blocktype> operator^(block<blocktype> const& one, block<blocktype> const& otr) {
    return block<blocktype>(one.b ^ otr.b);
}

template <typename blocktype>
inline block<blocktype> operator~(block<blocktype> const& one) {
    return block<blocktype>(~one.b);
}


/** Print the block, also in binary. First bit to the left.
 */
template <typename b_type>
std::ostream& operator<<(std::ostream& s, block<b_type> const & x) {
    return s << "block{" << x.b << "} = " << _detail::binary_of(x.b);
}

